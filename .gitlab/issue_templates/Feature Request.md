- [ ] I'm aware that all feature requests should be sent to the upstream: https://github.com/oasisfeng/island/issues/new/choose, unless
    - it's related to the build process or distribution; or
    - it's rejected by the upstream

    Other feature requests won't be processed

## Summary

(Summarize the the feature you want concisely)

## Why

(Incentives to add this feature)

## Implementation

(How this can be implemented, if possible to provide)

## Benefits

(Who and how this feature will benefit)

## Example Project

(Related projects with similar features)

## Alternatives

(Alternative approachs to satisfy your incentives, if any)

/label ~feature ~needs-investigation